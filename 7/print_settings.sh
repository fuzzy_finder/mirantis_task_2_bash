#!/bin/bash

FILE=$1 

# checking if there is available file to read from
if [ ! -r "$FILE" ];
then
    echo "There aren't any files to read from!"
    exit
fi

#read file
while read line
do
    $line # export variables from file
done < $FILE


#format output
echo \#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\# 

echo \#\# "Default login/password: $OS_USERNAME/$OS_PASSWORD"
echo \#\# "tenant: $OS_TENANT_NAME"

echo \#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\# 
