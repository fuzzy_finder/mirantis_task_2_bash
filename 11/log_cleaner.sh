#!/bin/bash

DIR_PATH=${1-$(pwd)}
cd $DIR_PATH || exit 1

# magic number - amount of seconds in one month (consider month = 30 days)
# not good aproach but suitable for this purpouse :)
let "MCS_IN_MONTH = 30 * 24 * 3600"

# now date in format 'time since the Epoch'
NOW_DATE_IN_MCS=$(date +%s)

for file in $(ls)
do
    FILE_ATIME=$(stat -c '%x' $file)
    # for the purpouse of correct processing by date util
    FILE_ATIME=${FILE_ATIME%.*}

    ATIME_IN_MCS=$(date +%s -d "$FILE_ATIME")
    
    let "TIME_DIFFERENCE=$NOW_DATE_IN_MCS-$ATIME_IN_MCS"

    if [ "$TIME_DIFFERENCE" -ge "$MCS_IN_MONTH" ]
    then
        rm $file
    fi
done
