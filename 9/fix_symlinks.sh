#!/bin/bash

DIR_PATH=${1-$(pwd)}
cd $DIR_PATH || exit 1


for file in $(ls)
do
    if [ -h $file -a ! -e $file ]
    then
        rm $file
    elif [ -h $file ]
    then
        LINKED_FILE=$(readlink $file)
        FILE_SIZE=$(stat -c '%s' $LINKED_FILE)

        if [ "$FILE_SIZE" -eq 0 ]
        then
            echo "WARNING: File '$LINKED_FILE' which is linked with '$file' is 0-size!!!"
        fi
    fi
done
