#!/bin/bash

# check whether input parameter (path to wirking directory) is set 
# if so - try to recursively create dir (if get error, scenario will terminate with error code 1)
# if first input parameter is not defined, variable takes pwd command return value
DIR_PATH=${1-$(pwd)}
cd $DIR_PATH 2> /dev/null || mkdir -p $DIR_PATH || exit 1 && cd $DIR_PATH

#creation of files
touch original.file # original file

for d in {0..10}
do
    touch ver${d}.file
done

#symlinks generating
LINK_SUFFIX=lnk
FILE_SUFFIX=file

for f in *.$FILE_SUFFIX
do
    LINK_NAME=${f%.$FILE_SUFFIX}
    ln -s $f $LINK_NAME.$LINK_SUFFIX
done

#file deleting
rm *.$FILE_SUFFIX
