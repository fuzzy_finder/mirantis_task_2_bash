#!/bin/bash

cat << end_of_message
------------------------
Script assignes tasks from
'tasks' file (which is passed as third argument)
for workers from 'workers.lst' file
in format "worker_name=task". 
Result is stored in 'work.table' file
Data in workers.lst is stored in form of 
sorted list of workers names from 'names' file
(second argument). Output files can be changed in 
setting.conf file.
------------------------
end_of_message

CONF_FILE=$1
NAMES_FILE=$2
TASKS_FILE=$3

# read conf file
while read line;
do
    $line
done < $CONF_FILE

while read line;
do
    # get length of name via awk
    name=$(echo $line | awk '{print $2}')
    last_name=$(echo $line | awk '{print $3}')
    let "name_length=${#name}+${#last_name}+1"
    
    awk -v name_length="$name_length" '{printf "[%s]: %s %s [%s]\n",$1,$2,$3,name_length}' < <(echo $line) >> $WORKERS_LIST
done < <(cat -n $NAMES_FILE | sort -k2)

# print tasks
echo "Current tasks:"
while read line;
do
    echo ${line/[[:space:]]/\_}
done < <(cat $TASKS_FILE)

# write into work.table file
awk -v date="$(date +"%Y/%m/%d")" -v time="$(date +"%H:%M:%S")" 'BEGIN {print "Assigned tasks:"}
NR==FNR{a[NR]=$2; b[NR]=$3; next}
{printf"%s %s=%s %s\n",a[FNR],b[FNR],$1,$2}
END{printf"\nDate: %s Time: %s\n",date,time}' $WORKERS_LIST $TASKS_FILE > $WORK_TABLE

echo -e "\nAssigning: DONE"
echo "Worker list: $WORKERS_LIST"
echo "Assigned task table: $WORK_TABLE"
