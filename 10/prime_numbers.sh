#!/bin/bash

usage(){
cat << EOF
Option -- Description"
-h -- print this help"
-n -- prime number to check (required)"
-N -- same as -n"
-f -- path to file for saving output"
-v -- verbose output"
EOF
}

# function
is_prime(){
    for ((number=2; number<$1; number++))
    do
        let "remainder=$1 % $number"
        
        if [ $remainder -eq 0 ]
        then
            if [ ! -z $VERBOSE_OUTPUT ]
            then
                echo "# ${1} is not prime: divides evently by $number"
            fi

            return 1
        fi
    done

    return 0
}

func(){
    echo "Prime number <= $1" | tee $SAVE_TO_FILE 
    for ((i=1; i<${1}; i++))
    do
        is_prime $i
        if [ $? -eq 0 ]
        then
            echo "$i" | tee -a $SAVE_TO_FILE
        fi
    done
}


USAGE=
SAVE_TO_FILE=
NUMBER_TO_PROCESS=
VERBOSE_OUTPUT=
while getopts ":hn:N:f:v" opt;
do
    case $opt in 
        h)
            USAGE=$OPTIND
            ;;
        n | N)  
            NUMBER_TO_PROCESS=$OPTARG
            ;;
        f)
            SAVE_TO_FILE=$OPTARG
            ;;
        v)
            VERBOSE_OUTPUT=$OPTIND
    esac
done
shift $(($OPTIND - 1))

if [ -z $NUMBER_TO_PROCESS ]
then
    echo "ERROR: Number for processing is missing"
    exit 1
else
    if [ ! -z $SAVE_TO_FILE ]
    then
        path_to_file=${SAVE_TO_FILE%\*}
        file_name=${SAVE_TO_FILE##*\/}

        touch $SAVE_TO_FILE 2> /dev/null || cd $path_to_file 2> /dev/null || (mkdir -p $path_to_file && touch $SAVE_TO_FILE) 2> /dev/null || { echo "ERROR:
        Something is wrong with path to file" && exit 1; }

        if [ ! -z $VERBOSE_OUTPUT ]
        then 
            echo "saving results to file: $SAVE_TO_FILE"
        fi

    fi

    func $NUMBER_TO_PROCESS
fi

if [ ! -z $USAGE ]
then
    usage
fi
